package com.troytamas.japanslate;

import android.content.Context;
import android.database.Cursor;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.troytamas.japanslate", appContext.getPackageName());
    }

    @Test
    public void testEntriesRetrieved() {
        Context c = InstrumentationRegistry.getTargetContext();
        DbObject dbo = new DbObject(c);
//    String columns[] = {"gloss"};
        String searchTerm = "津";
        Cursor cursor = dbo.getMatches(searchTerm);
        int nResults = cursor.getCount();
        assert nResults > 1;
        cursor.moveToFirst();
        int iKanji = cursor.getColumnIndex("kanji");
        String kanji = cursor.getString(iKanji);
        assert kanji.contains(searchTerm);
    }

    @Test
    public void testNoMatchReturnsEmptyCursor() {
        Context c = InstrumentationRegistry.getTargetContext();
        DbObject dbo = new DbObject(c);
//    String columns[] = {"gloss"};
        String searchTerm = "津饅頭";
        Cursor cursor = dbo.getMatches(searchTerm);
        assertNull(cursor);
    }
}
