package com.troytamas.japanslate;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * Created by troyt on 6/15/2017.
 */

public class TranslationDatabase extends SQLiteAssetHelper {

    private static final String DATABASE_NAMES = "jmdict.db";
    private static final int DATABASE_VERSION = 1;

    public TranslationDatabase(Context context) {
        super(context, DATABASE_NAMES, null, DATABASE_VERSION);
    }
}
