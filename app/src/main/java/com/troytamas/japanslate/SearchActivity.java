package com.troytamas.japanslate;

import android.app.ListActivity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.Contacts;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ListAdapter;
import android.widget.SimpleCursorAdapter;

import static android.widget.CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER;


public class SearchActivity extends ListActivity {
    private Cursor mCursor;
    private DbObject dbo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_search);
        // Enable type-to-search
        setDefaultKeyMode(DEFAULT_KEYS_SEARCH_LOCAL);
        handleIntent(getIntent());
        dbo = new DbObject(getApplicationContext());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            doSearch(query);
        }
    }

    private void doSearch(String queryStr) {
        // get a Cursor, prepare the ListAdapter and set it
        mCursor = dbo.getMatches(queryStr);

        ListAdapter adapter = new SimpleCursorAdapter(
                this, // Context
                android.R.layout.two_line_list_item, // Row template
                mCursor, // Cursor to bind to
                new String[]{"kanji", "gloss"}, // Array of cursor columns to bind to
                new int[]{android.R.id.text1, android.R.id.text2},
                FLAG_REGISTER_CONTENT_OBSERVER
        ) {
            @Override
            public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
                return null;
            }

            @Override
            public void bindView(View view, Context context, Cursor cursor) {

            }
        }; // Parallel array of which template objects to bind to those columns
        // bind to the adapter
        setListAdapter(adapter);
    }

}
