package com.troytamas.japanslate;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class GoScreen extends AppCompatActivity {
    public final static String EXTRA_MESSAGE = "com.troytamas.japanslate.MESSAGE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_go_screen);
    }

    public void onGoClick(View view) {
//        Intent intent = new Intent(this, OnCopyNotification.class);
//        String message = "Hello, world!";
//        intent.putExtra(EXTRA_MESSAGE, message);
//        startActivity(intent);
        OnCopyNotification.notify(this, null, 5);
    }
}
