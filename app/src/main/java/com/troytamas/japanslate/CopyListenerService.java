package com.troytamas.japanslate;

import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.IBinder;
import android.util.Log;

public class CopyListenerService extends Service {
    private DbObject dbo;
    public CopyListenerService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    @Override
    public void onCreate() {
        Log.v("CopyListenerService", "Started CopyListenerService");
        ClipboardManager cpm = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        Context context = getApplicationContext();
        dbo = new DbObject(context);
        cpm.addPrimaryClipChangedListener(new NotifyClipboardContentChanged(context, cpm, dbo));
    }

}

class NotifyClipboardContentChanged implements ClipboardManager.OnPrimaryClipChangedListener {
    private final Context context;
    private DbObject dbo;
    public NotifyClipboardContentChanged(Context context, ClipboardManager cpm, DbObject dbo) {
        super();
        this.cpm = cpm;
        this.context = context;
        this.dbo = dbo;
    }

    private ClipboardManager cpm;
    @Override
    public void onPrimaryClipChanged() {
        Log.v("onPrimaryClipChanged", "handling event");
        ClipData content = cpm.getPrimaryClip();
        if (content != null) {
            ClipData.Item item = content.getItemAt(0);
            CharSequence text = item.getText();
            Cursor cursor = dbo.getMatches(text.toString());
            if (cursor != null) {
//                String gloss = cursor.getString(2);
                OnCopyNotification.notify(context, cursor, 1);
            }
        }
    }
}
