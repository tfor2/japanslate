package com.troytamas.japanslate;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Troy on 2016/11/29.
 */

public class autostart extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent copyListenerIntent = new Intent(context, CopyListenerService.class);
        context.startService(copyListenerIntent);
        Log.i("Autostart", "started");
    }
}
