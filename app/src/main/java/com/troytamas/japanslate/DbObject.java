package com.troytamas.japanslate;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

/**
 * Created by troyt on 6/15/2017.
 */

public class DbObject {
    private static final String FTS_VIRTUAL_TABLE = "entries";
    private static final String COL_ENTRY = "entries";
    protected final TranslationDatabase dbHelper;
    private SQLiteDatabase db;

    public DbObject(Context context) {
        dbHelper = new TranslationDatabase(context);
        this.db = dbHelper.getReadableDatabase();
    }

    public Cursor getMatches(String word) {
        return getMatches(word, null);
    }
    public Cursor getMatches(String word, String[] columns) {
        return getMatches(word, COL_ENTRY, columns);
    }
    public Cursor getMatches(String word, String matchCol, String[] columns) {
        String selection = matchCol + " MATCH ?";
        String[] selectionArgs = new String[] {word+"*"};
        return query(selection, selectionArgs, columns);
    }

    public Cursor query(String selection, String[] selectionArgs, String[] columns) {
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(FTS_VIRTUAL_TABLE);

        Cursor cursor = builder.query(this.dbHelper.getReadableDatabase(),
                columns, selection, selectionArgs, null, null, null);

        if (cursor == null) {
            return null;
        } else if (!cursor.moveToFirst()) {
            cursor.close();
            return null;
        }
        return cursor;
    }

    public SQLiteDatabase getDbConnection(){
        return this.db;
    }

    public void closeDbConnection(){
        if(this.db != null){
            this.db.close();
        }
    }
}
